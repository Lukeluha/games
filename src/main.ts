process.env.NODE_CONFIG_DIR = __dirname + '/config';

import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import config from 'mikro-config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
    .setTitle('Games API')
    .setDescription('This API provides endpoints for getting info about games deals')
    .setVersion('0.1')
    .addTag('games')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('docs', app, document);

  await app.listen(config.get('server.port'));
}
bootstrap();
