import { Module } from '@nestjs/common';
import { GameModule } from './modules/game/game.module';
import { CoreModule } from './modules/core/core.module';

@Module({
  imports: [
    GameModule,
    CoreModule,
  ],
})
export class AppModule {}
