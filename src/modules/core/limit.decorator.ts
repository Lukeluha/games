import { createParamDecorator } from '@nestjs/common';

export const Limit = createParamDecorator((data, req) => {
  let limit = 10;

  if (req.query && req.query.limit) {
    limit = parseInt(req.query.limit, 10);
  }

  return limit;
});