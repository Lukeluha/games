import { Module } from '@nestjs/common';
import Axios from 'axios';

const providers = [
    {
      provide: 'Axios',
      useValue: Axios,
    },
];

@Module({
  providers,
  exports: [...providers],
})
export class CoreModule {}
