import { ApiModelProperty } from '@nestjs/swagger';

export class Game {
  @ApiModelProperty()
  readonly name: string;

  @ApiModelProperty()
  readonly salePrice: number;

  @ApiModelProperty()
  readonly cheapestPrice: number;

  @ApiModelProperty({ example: Math.floor(new Date().getTime() / 1000), type: Number, format: 'epoch' })
  readonly releaseDate: Date;
}