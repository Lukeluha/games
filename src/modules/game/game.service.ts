import { Inject, Injectable } from '@nestjs/common';
import { ICheapSharkGame } from './interfaces/cheapSharkGame.interface';
import { ICheapSharkDeal } from './interfaces/cheapSharkDeal.interface';
import { Game } from './game.entity';
import { AxiosStatic } from 'axios';

export const baseUrl = 'http://www.cheapshark.com/api/1.0';

@Injectable()
export class GameService {
  constructor(
    @Inject('Axios')
    private readonly http: AxiosStatic,
  ) {}

  /**
   * Returns list of games with the cheapest deals
   * @param gameTitleQuery
   * @param limit
   */
  public async getCheapestDeals(gameTitleQuery: string, limit: number): Promise<Game[]> {
    const gamesInfoList = await this.fetchGamesInfo(gameTitleQuery, limit);

    if (!gamesInfoList) {
      return [];
    }

    return await Promise.all(
      gamesInfoList.map( async (gameInfo) => {
        const dealInfo = await this.fetchDeal(gameInfo.cheapestDealID);

        return {
          name: dealInfo.gameInfo.name,
          salePrice: +dealInfo.gameInfo.salePrice,
          cheapestPrice: +dealInfo.cheapestPrice.price,
          releaseDate: dealInfo.gameInfo.releaseDate || null,
        };
      }),
    );
  }

  /**
   * Return info about game from cheapshark API.
   * It is used mainly for getting data game by query and their cheapest deals
   * @param gameTitleQuery
   * @param limit
   */
  private async fetchGamesInfo(gameTitleQuery: string, limit: number): Promise<ICheapSharkGame[]> {
    const res = await this.http.get<ICheapSharkGame[]>(`${baseUrl}/games`, {
      params: {
        title: gameTitleQuery,
        limit,
      },
    });

    if (res.status !== 200 && res.data) {
      throw new Error('Error fetching games data');
    }

    return res.data;
  }

  private async fetchDeal(id: string): Promise<ICheapSharkDeal> {
    const res = await this.http.get<ICheapSharkDeal>(`${baseUrl}/deals`, {
      params: {
        id: decodeURIComponent(id),
      },
    });

    if (res.status !== 200 && res.data) {
      throw new Error('Error fetching deal data');
    }

    return res.data;
  }
}
