export interface ICheapSharkGame {
  readonly gameID: string;
  readonly steamAppID?: string;
  readonly cheapest: string;
  readonly cheapestDealID: string;
  readonly external: string;
  readonly thumb: string;
}