import { ICheapSharkGameInfo } from './cheapSharkGameInfo.interface';

export interface ICheapSharkDeal {
  readonly gameInfo: ICheapSharkGameInfo;
  readonly cheaperStores: [];
  readonly cheapestPrice: {
    readonly price: string;
    readonly date: Date;
  };
}