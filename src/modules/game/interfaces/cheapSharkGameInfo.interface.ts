export interface ICheapSharkGameInfo {
  readonly storeID: string;
  readonly gameID: string;
  readonly name: string;
  readonly steamAppID?: string;
  readonly salePrice: string;
  readonly retailPrice: string;
  readonly steamRatingText?: string;
  readonly steamRatingPercent?: string;
  readonly steamRatingCount: string;
  readonly metacriticScore: string;
  readonly metacriticLink: string;
  readonly releaseDate: Date;
  readonly publisher: string;
  readonly steamworks: string;
  readonly thumb: string;
}