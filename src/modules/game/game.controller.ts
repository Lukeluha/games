import { BadRequestException, Controller, Get, Query } from '@nestjs/common';
import { ApiUseTags, ApiOperation, ApiImplicitQuery, ApiResponse } from '@nestjs/swagger';
import { GameService } from './game.service';
import { Game } from './game.entity';
import { Limit } from '../core/limit.decorator';

@Controller('games')
@ApiUseTags('games')
export class GameController {

  constructor(
    private readonly gameService: GameService,
  ) {}

  @Get('/')
  @ApiOperation({ title: 'Find games with the cheapest price ever and current cheapest price' })
  @ApiImplicitQuery({ name: 'title', type: String, required: true, description: 'Game title query' })
  @ApiImplicitQuery({ name: 'limit', type: Number, required: false, description: 'Limit of games in result' })
  @ApiResponse({ status: 200, type: Game, isArray: true })
  async getGameInfo(@Query('title') gameTitleQuery: string, @Limit() limit: number): Promise<Game[]> {
    if (!gameTitleQuery) {
      throw new BadRequestException('Title must be filled');
    }

    return await this.gameService.getCheapestDeals(gameTitleQuery, limit);
  }
}
