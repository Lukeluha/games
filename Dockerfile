FROM node:10.15.0
# LTS version

ARG NPM_OPTION=""

WORKDIR /usr/src/app

# package dependencies
COPY package-lock.json ./
COPY package.json ./


RUN npm install $NPM_OPTION

COPY . ./

RUN npm run build

CMD ["npm", "run", "start:prod"]