export default [
  {
    gameID: '69',
    steamAppID: '901583',
    cheapest: '8.99',
    cheapestDealID: 'deal_1',
    external: 'Grand Theft Auto IV: Complete Edition',
    thumb: 'https://steamcdn-a.akamaihd.net/steam/apps/901583/capsule_sm_120.jpg?t=1509997232',
  },
  {
    gameID: '96',
    steamAppID: '12210',
    cheapest: '6.99',
    cheapestDealID: 'deal_2',
    external: 'Grand Theft Auto IV',
    thumb: 'https://steamcdn-a.akamaihd.net/steam/apps/12210/capsule_sm_120.jpg?t=1505332041',
  },
  {
    gameID: '108',
    steamAppID: '12110',
    cheapest: '3.49',
    cheapestDealID: 'deal_3',
    external: 'Grand Theft Auto: Vice City',
    thumb: 'https://steamcdn-a.akamaihd.net/steam/apps/12110/capsule_sm_120.jpg?t=1543431247',
  },
];