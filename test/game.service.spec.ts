import { Test, TestingModule } from '@nestjs/testing';
import { GameService } from '../src/modules/game/game.service';
import { CoreModule } from '../src/modules/core/core.module';
import { AxiosStatic } from 'axios';
import { get } from './mocks/axiosGames.mock';

describe('GameService', () => {
  let service: GameService;
  let http: AxiosStatic;
  let result;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CoreModule],
      providers: [GameService],
    }).compile();

    service = module.get<GameService>(GameService);
    http = module.get<AxiosStatic>('Axios');

    // set mocks
    jest.spyOn(http, 'get')
      .mockImplementation(get);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return info about games deals', async () => {
    result = await service.getCheapestDeals('grand theft auto', 5);
    expect(result).toEqual([
      {
        name: 'Grand Theft Auto IV: Complete Edition',
        salePrice: 29.99,
        cheapestPrice: 5.99,
        releaseDate: null,
      },
      {
        name: 'Grand Theft Auto V',
        salePrice: 29.99,
        cheapestPrice: 6.99,
        releaseDate: null,
      },
      {
        name: 'Grand Theft Auto III: Complete Edition',
        salePrice: 29.99,
        cheapestPrice: 7.99,
        releaseDate: null,
      },
    ]);
  });

  it('should limit the result', async () => {
    result = await service.getCheapestDeals('grand theft auto', 1);
    expect(result).toHaveLength(1);
  });

  it('should deal with non-existing game', async () => {
    result = await service.getCheapestDeals('blahblah', 5);
    expect(result).toEqual([]);
  });
});
