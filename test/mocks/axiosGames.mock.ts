import { AxiosRequestConfig } from 'axios';
import games from '../fixtures/games.fixture';
import deals from '../fixtures/deals.fixture';

export function get(url: string, config?: AxiosRequestConfig) {
  if (url.endsWith('games')) { // fetch game info
    if (config.params.title === 'blahblah') {
      return {
        status: 200,
        data: [],
      };
    } else {
      return {
        status: 200,
        data: games.slice(0, config.params.limit || 10),
      };
    }
  } else if (url.endsWith('deals') && config.params.id) { // fetch deal info
    return {
      status: 200,
      data: deals[config.params.id],
    };
  }
}