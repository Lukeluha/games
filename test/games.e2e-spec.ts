import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { GameModule } from '../src/modules/game/game.module';
import { AxiosStatic } from 'axios';
import { get } from './mocks/axiosGames.mock';

describe('GamesController', () => {
  let app: INestApplication;
  let server: any;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [GameModule],
    }).compile();

    const http = moduleFixture.get<AxiosStatic>('Axios');

    // set mocks
    jest.spyOn(http, 'get')
      .mockImplementation(get);

    app = moduleFixture.createNestApplication();
    await app.init();
    server = app.getHttpServer();
  });

  describe('GET /', () => {
    it('should required game title', () => {
      return request(server)
        .get('/games')
        .expect(400);
    });

    it('should reject empty title', () => {
      return request(server)
        .get('/games?title=')
        .expect(400);
    });

    it('should fetch info about games deals', () => {
      return request(server)
        .get(`/games?title=${decodeURIComponent('grand theft auto')}`)
        .expect(200)
        .expect((res) => {
          expect(res.body).not.toHaveLength(0);
        });
    });

    it('should limit the result by url param', () => {
      return request(server)
        .get(`/games?title=${decodeURIComponent('grand theft auto')}&limit=1`)
        .expect(200)
        .expect((res) => {
          expect(res.body).toHaveLength(1);
        });
    });
  });
});
